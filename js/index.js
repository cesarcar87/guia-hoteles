$(function() {
    //Se inicializa el tooltip
    $("[data-toggle='tooltip']").tooltip();

    //Se inicializa el popover
    $("[data-toggle='popover']").popover();

    //Se le estabece un tiempo de intervalo entre slide del carousel
    $('.carousel').carousel({
        interval: 2000
    });

     //***Eventos del componente modal***//

     //Eventos show y shown
    $('#contacto').on('show.bs.modal', function(e) {
        console.log('El modal de contacto se está mostrando.');

        //Se desactiva el botón del modal al comenzar a mostrarse el mismo
        $("[data-target='#contacto']").prop('disabled', true);
    });
    $('#contacto').on('shown.bs.modal', function(e) {
        console.log('El modal de contacto se mostró.');
    });

    //Eventos hide y hidden
    $('#contacto').on('hide.bs.modal', function(e) {
        console.log('El modal de contacto se está ocultando.');
    });
    $('#contacto').on('hidden.bs.modal', function(e) {
        console.log('El modal de contacto se ocultó.');

        //Se vuelve a activar el botón del modal al ocultarse el mismo
        $("[data-target='#contacto']").prop('disabled', false);
    });
});